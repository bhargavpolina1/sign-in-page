let sucessMessageEle = document.getElementById("sucessMessage");
let firstNameEle = document.getElementById("firstName");
let lastNameEle = document.getElementById("lastName");
let emailIdEle = document.getElementById("emailId");
let passwordEle = document.getElementById("password");
let reEnterPasswordEle = document.getElementById("reEnterPassword");
let InputCheckBoxEle = document.getElementById("InputCheckBox");
let submitBtnEle = document.getElementById("submitBtn");
let signInFormEle = document.getElementById("signInForm");
let headingContainerEle = document.getElementById("headingContainer");

let firstNameErrorEle = document.getElementById("firstNameError");
let lastNameErrorEle = document.getElementById("lastNameError");
let emailErrorEle = document.getElementById("emailError");
let EnterPasswordErrorEle = document.getElementById("EnterPasswordError");
let reEnterPasswordErrorEle = document.getElementById("reEnterPasswordError");
let agreeTermsEle = document.getElementById("agreeTerms");

let isFirstNameValid = false;
let isLastNameValid = false;
let isEmailValid = false;
let IsPasswordValid = false;
let isPassWordMatched = false;
let isTermsAcceptd = false;

let aValidPassword = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
let isValidName = /^[A-Za-z]+$/;
let isValidEmail = /^\w+([.-]?\w+)@\w+([.-]?\w+)(.\w{2,})+$/;

window.onload = function () {
  firstNameEle.value = "";
  lastNameEle.value = "";
  emailIdEle.value = "";
  passwordEle.value = "";
  reEnterPasswordEle.value = "";
  InputCheckBoxEle.checked = false;
};

submitBtnEle.addEventListener("click", () => {
  if (firstNameEle.value === "") {
    firstNameErrorEle.textContent = "*First name required";
  } else if (!isValidName.test(firstNameEle.value)) {
    firstNameErrorEle.textContent = "*Only letters in first name";
  } else {
    firstNameErrorEle.textContent = "";
    isFirstNameValid = true;
  }

  if (lastNameEle.value === "") {
    lastNameErrorEle.textContent = "*Last name required";
  } else if (!isValidName.test(lastNameEle.value)) {
    lastNameErrorEle.textContent = "*Only letters in last name";
  } else {
    lastNameErrorEle.textContent = "";
    isLastNameValid = true;
  }

  if (emailIdEle.value === "") {
    emailErrorEle.textContent = "*Email-id required";
  } else if (!emailIdEle.value.match(isValidEmail)) {
    emailErrorEle.textContent = "*Enter valid Email";
  } else {
    emailErrorEle.textContent = "";
    isEmailValid = true;
  }

  if (!passwordEle.value.match(aValidPassword)) {
    EnterPasswordErrorEle.textContent = "*Enter a valid password";
  } else {
    EnterPasswordErrorEle.textContent = "";
    IsPasswordValid = true;
  }

  if (reEnterPasswordEle.value === "") {
    reEnterPasswordErrorEle.textContent = "*Re-enter password";
  } else if (reEnterPasswordEle.value !== passwordEle.value) {
    reEnterPasswordErrorEle.textContent = "*Passwords didn't match";
  } else {
    reEnterPasswordErrorEle.textContent = "";
    isPassWordMatched = true;
  }

  if (InputCheckBoxEle.checked === false) {
    agreeTermsEle.textContent = "* Please accept T&C";
  } else {
    agreeTermsEle.textContent = "";
    isTermsAcceptd = true;
  }

  if (
    isFirstNameValid &&
    isLastNameValid &&
    isEmailValid &&
    IsPasswordValid &&
    isPassWordMatched &&
    isTermsAcceptd
  ) {
    signInFormEle.style.visibility = "hidden";
    let createHeading = document.createElement("h1");
    createHeading.textContent = "Account Created Sucessfully";
    createHeading.id = "sucessMessage";
    headingContainerEle.appendChild(createHeading);
  }
});
